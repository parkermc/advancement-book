package net.parkermc.advancementbook;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber
public class AdvancementBookConfig {

    public static final String CATEGORY_GENERAL = "general";
    public static ForgeConfigSpec.BooleanValue GIVE_FIRST_JOIN;


    private static final ForgeConfigSpec.Builder BUILDER = new ForgeConfigSpec.Builder();
    public static ForgeConfigSpec SPEC;

    static {
        BUILDER.comment("General settings").push(CATEGORY_GENERAL);
        GIVE_FIRST_JOIN = BUILDER.comment("Enables giving everyone a book the first time they join").define("giveFirstJoin", true);
        BUILDER.pop();

        SPEC = BUILDER.build();
    }
}
