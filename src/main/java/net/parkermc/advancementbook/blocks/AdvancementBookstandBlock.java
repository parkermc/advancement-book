package net.parkermc.advancementbook.blocks;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.advancements.AdvancementsScreen;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.parkermc.advancementbook.blockentities.AdvancementBookBlockEntities;
import net.parkermc.advancementbook.blockentities.AdvancementBookstandBlockEntity;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

public class AdvancementBookstandBlock extends BaseEntityBlock {
    protected static final VoxelShape SHAPE = Block.box(0.0D, 0.0D, 0.0D, 16.0D, 12.0D, 16.0D);

   public AdvancementBookstandBlock(Block.Properties props) {
      super(props);
   }
   
   @Override
   @ParametersAreNonnullByDefault
   @SuppressWarnings("deprecation")
   public boolean useShapeForLightOcclusion(BlockState blockState) {
       return true;
   }

   @Override
   @SuppressWarnings("deprecation")
   @Nonnull
   @ParametersAreNonnullByDefault
   public VoxelShape getShape(BlockState blockState, BlockGetter blockGetter, BlockPos p_52990_, CollisionContext p_52991_) {
       return SHAPE;
   }

   @Override
   @Nonnull
   @ParametersAreNonnullByDefault
   public RenderShape getRenderShape(BlockState blockState) {
       return RenderShape.MODEL;
   }

   @Override
   @ParametersAreNonnullByDefault
   public BlockEntity newBlockEntity(BlockPos pos, BlockState blockState) {
      return new AdvancementBookstandBlockEntity(pos, blockState);
   }


   @Override
   @SuppressWarnings("deprecation")
   @Nonnull
   @ParametersAreNonnullByDefault
   public InteractionResult use(BlockState blockState, Level level, BlockPos p_52976_, Player p_52977_, InteractionHand p_52978_, BlockHitResult p_52979_) {
		if(level.isClientSide()) {
			DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> this::openAdvancementScreen); // Used this so we can return success server side
		}
		return InteractionResult.SUCCESS;
   }

   @Override
   @ParametersAreNonnullByDefault
   public void setPlacedBy(Level level, BlockPos blockPos, BlockState blockState, @Nullable LivingEntity entity, ItemStack stack) {
      if (stack.hasCustomHoverName()) {
          BlockEntity blockEntity = level.getBlockEntity(blockPos);
         if (blockEntity instanceof AdvancementBookstandBlockEntity) {
            ((AdvancementBookstandBlockEntity)blockEntity).setCustomName(stack.getHoverName());
         }
      }
   }

   @Override
   @Nullable
   @ParametersAreNonnullByDefault
   public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level level, BlockState blockState, BlockEntityType<T> blockEntityType) {
       return level.isClientSide ? createTickerHelper(blockEntityType, AdvancementBookBlockEntities.ADVANCEMENT_BOOKSTAND_BLOCK_ENTITY.get(), AdvancementBookstandBlockEntity::bookAnimationTick) : null;
   }
   
	@OnlyIn(Dist.CLIENT)
	private void openAdvancementScreen() {
		Minecraft mc = Minecraft.getInstance();
        if(mc.player == null){
            return;
        }
		mc.setScreen(new AdvancementsScreen(mc.player.connection.getAdvancements()));
	}
   
}
