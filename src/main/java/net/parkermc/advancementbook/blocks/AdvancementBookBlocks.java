package net.parkermc.advancementbook.blocks;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.material.MaterialColor;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.ObjectHolder;
import net.minecraftforge.registries.RegistryObject;
import net.parkermc.advancementbook.AdvancementBookMod;

public class AdvancementBookBlocks {
	public static final DeferredRegister<Block> BLOCKS_REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCKS, AdvancementBookMod.MODID);

	public static final RegistryObject<AdvancementBookstandBlock> ADVANCEMENT_BOOKSTAND =
			BLOCKS_REGISTRY.register("advancement_bookstand", () ->
					new AdvancementBookstandBlock(Block.Properties.of(Material.WOOD, MaterialColor.COLOR_RED).strength(1.5F, 5.0F))
			);
}
