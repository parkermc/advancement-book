package net.parkermc.advancementbook;

import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.parkermc.advancementbook.blockentities.AdvancementBookBlockEntities;
import net.parkermc.advancementbook.blocks.AdvancementBookBlocks;
import net.parkermc.advancementbook.items.AdvancementBookItems;

import javax.annotation.Nonnull;

@Mod(AdvancementBookMod.MODID)
public class AdvancementBookMod{
    public static final String MODID = "advancementbook";
    public static CreativeModeTab CREATIVE_TAB = new CreativeModeTab("advancement_book") {

		@Override
        @Nonnull
		public ItemStack makeIcon() {
			return new ItemStack(AdvancementBookItems.ADVANCEMENT_BOOK.get());
		}
	};

    public AdvancementBookMod() {
        // Registries
        AdvancementBookBlocks.BLOCKS_REGISTRY.register(FMLJavaModLoadingContext.get().getModEventBus());
        AdvancementBookItems.ITEMS_REGISTRY.register(FMLJavaModLoadingContext.get().getModEventBus());
        AdvancementBookBlockEntities.BLOCK_ENTITY_REGISTRY.register(FMLJavaModLoadingContext.get().getModEventBus());

        // Register config stuff
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, AdvancementBookConfig.SPEC);
    }
}

