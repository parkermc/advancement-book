package net.parkermc.advancementbook.items;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.advancements.AdvancementsScreen;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

public class AdvancementBookItem extends Item {
	
	public AdvancementBookItem(Item.Properties props) {
		super(props);
	}


	@Override
	@Nonnull
	@ParametersAreNonnullByDefault
	public InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand hand) {
		if(level.isClientSide()) {
			DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> this::openAdvancementScreen); // Used this so we can return success server side
		}
		return new InteractionResultHolder<>(InteractionResult.SUCCESS, player.getItemInHand(hand));
	}
	
	@OnlyIn(Dist.CLIENT)
	private void openAdvancementScreen() {
		Minecraft mc = Minecraft.getInstance();
		if(mc.player == null){
			return;
		}
		mc.setScreen(new AdvancementsScreen(mc.player.connection.getAdvancements()));
	}
	
}
