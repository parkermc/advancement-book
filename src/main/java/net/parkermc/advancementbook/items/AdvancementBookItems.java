package net.parkermc.advancementbook.items;

import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.parkermc.advancementbook.AdvancementBookMod;
import net.parkermc.advancementbook.blocks.AdvancementBookBlocks;

public class AdvancementBookItems {
	public static final DeferredRegister<Item> ITEMS_REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, AdvancementBookMod.MODID);

	public static final RegistryObject<AdvancementBookItem> ADVANCEMENT_BOOK =
			ITEMS_REGISTRY.register("advancement_book", () ->
					new AdvancementBookItem(new Item.Properties().tab(AdvancementBookMod.CREATIVE_TAB).stacksTo(1))
			);

	@SuppressWarnings("unused")
	public static final RegistryObject<BlockItem> ADVANCEMENT_BOOKSTAND =
			ITEMS_REGISTRY.register(AdvancementBookBlocks.ADVANCEMENT_BOOKSTAND.getId().getPath(), () ->
				new BlockItem(AdvancementBookBlocks.ADVANCEMENT_BOOKSTAND.get(), new Item.Properties().tab(AdvancementBookMod.CREATIVE_TAB))
			);
}
