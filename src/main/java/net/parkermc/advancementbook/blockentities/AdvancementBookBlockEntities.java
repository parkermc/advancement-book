package net.parkermc.advancementbook.blockentities;

import net.minecraft.client.renderer.blockentity.BlockEntityRenderers;
import net.minecraft.client.renderer.texture.TextureAtlas;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.parkermc.advancementbook.AdvancementBookMod;
import net.parkermc.advancementbook.blocks.AdvancementBookBlocks;

@Mod.EventBusSubscriber(modid = AdvancementBookMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class AdvancementBookBlockEntities {

	public static final DeferredRegister<BlockEntityType<?>> BLOCK_ENTITY_REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCK_ENTITIES, AdvancementBookMod.MODID);


	@SuppressWarnings("ConstantConditions")
	public static final RegistryObject<BlockEntityType<AdvancementBookstandBlockEntity>> ADVANCEMENT_BOOKSTAND_BLOCK_ENTITY =
			BLOCK_ENTITY_REGISTRY.register("advancement_bookstand_tile_entity", () ->
					BlockEntityType.Builder.of(AdvancementBookstandBlockEntity::new, AdvancementBookBlocks.ADVANCEMENT_BOOKSTAND.get()).build( null)
			);

	
	@SuppressWarnings("deprecation")
	@OnlyIn(Dist.CLIENT)
	@SubscribeEvent
	public static void onTextureStitchEvent(TextureStitchEvent.Pre event) {
		if (event.getAtlas().location().equals(TextureAtlas.LOCATION_BLOCKS)){
			event.addSprite(AdvancementBookstandBlockEntityRenderer.TEXTURE_BOOK_LOCATION);
		}
	}
	
	@SubscribeEvent
	@OnlyIn(Dist.CLIENT)
    public static void clientSetup(FMLClientSetupEvent event) {
		// Register the renderers
		BlockEntityRenderers.register(ADVANCEMENT_BOOKSTAND_BLOCK_ENTITY.get(), AdvancementBookstandBlockEntityRenderer::new);
    }
}
