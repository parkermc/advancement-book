package net.parkermc.advancementbook.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.util.Mth;
import net.minecraft.world.Nameable;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Random;

// This is an almost straight copy from the enchantment table tile entity 
public class AdvancementBookstandBlockEntity extends BlockEntity implements Nameable {
   public int time;
   public float flip;
   public float oFlip;
   public float flipT;
   public float flipA;
   public float open;
   public float oOpen;
   public float rot;
   public float oRot;
   public float tRot;
   private static final Random RANDOM = new Random();
   private Component name;

   public AdvancementBookstandBlockEntity(BlockPos blockPos, BlockState blockState) {
      super(AdvancementBookBlockEntities.ADVANCEMENT_BOOKSTAND_BLOCK_ENTITY.get(), blockPos, blockState);
   }
   
   @Override
   @Nonnull
   @ParametersAreNonnullByDefault
   public CompoundTag save(CompoundTag compoundTag) {
      super.save(compoundTag);
      if (this.hasCustomName()) {
         compoundTag.putString("CustomName", Component.Serializer.toJson(this.name));
      }

      return compoundTag;
   }

   @Override
   @ParametersAreNonnullByDefault
   public void load(CompoundTag compoundTag) {
      super.load(compoundTag);
      if (compoundTag.contains("CustomName", 8)) {
         this.name = Component.Serializer.fromJson(compoundTag.getString("CustomName"));
      }

   }

   public static void bookAnimationTick(Level level, BlockPos blockPos, @SuppressWarnings("unused") BlockState blockState, AdvancementBookstandBlockEntity blockEntity) {
      blockEntity.oOpen = blockEntity.open;
      blockEntity.oRot = blockEntity.rot;
      Player player = level.getNearestPlayer((double)blockPos.getX() + 0.5D, (double)blockPos.getY() + 0.5D, (double)blockPos.getZ() + 0.5D, 3.0D, false);
      if (player != null) {
         double d0 = player.getX() - ((double)blockPos.getX() + 0.5D);
         double d1 = player.getZ() - ((double)blockPos.getZ() + 0.5D);
         blockEntity.tRot = (float) Mth.atan2(d1, d0);
         blockEntity.open += 0.1F;
         if (blockEntity.open < 0.5F || RANDOM.nextInt(40) == 0) {
            float f1 = blockEntity.flipT;

            do {
               blockEntity.flipT += (float)(RANDOM.nextInt(4) - RANDOM.nextInt(4));
            } while(f1 == blockEntity.flipT);
         }
      } else {
         blockEntity.tRot += 0.02F;
         blockEntity.open -= 0.1F;
      }

      while(blockEntity.rot >= (float)Math.PI) {
         blockEntity.rot -= ((float)Math.PI * 2F);
      }

      while(blockEntity.rot < -(float)Math.PI) {
         blockEntity.rot += ((float)Math.PI * 2F);
      }

      while(blockEntity.tRot >= (float)Math.PI) {
         blockEntity.tRot -= ((float)Math.PI * 2F);
      }

      while(blockEntity.tRot < -(float)Math.PI) {
         blockEntity.tRot += ((float)Math.PI * 2F);
      }

      float f2;
      for(f2 = blockEntity.tRot - blockEntity.rot; f2 >= (float)Math.PI;) {
         f2 -= ((float)Math.PI * 2F);
      }

      while(f2 < -(float)Math.PI) {
         f2 += ((float)Math.PI * 2F);
      }

      blockEntity.rot += f2 * 0.4F;
      blockEntity.open = Mth.clamp(blockEntity.open, 0.0F, 1.0F);
      ++blockEntity.time;
      blockEntity.oFlip = blockEntity.flip;
      float f = (blockEntity.flipT - blockEntity.flip) * 0.4F;
      f = Mth.clamp(f, -0.2F, 0.2F);
      blockEntity.flipA += (f - blockEntity.flipA) * 0.9F;
      blockEntity.flip += blockEntity.flipA;
   }

   @Override
   @Nonnull
   public Component getName() {
      return this.name != null ? this.name : new TranslatableComponent("container.enchant");
   }

   public void setCustomName(@Nullable Component customName) {
      this.name = customName;
   }

   @Override
   @Nullable
   public Component getCustomName() {
      return this.name;
   }
}