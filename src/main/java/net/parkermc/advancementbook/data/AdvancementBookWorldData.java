package net.parkermc.advancementbook.data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.saveddata.SavedData;
import net.minecraft.world.level.storage.DimensionDataStorage;
import net.parkermc.advancementbook.AdvancementBookMod;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

public class AdvancementBookWorldData extends SavedData {
	private static final String DATA_NAME = AdvancementBookMod.MODID;

	public List<UUID> givenBook = new ArrayList<>();

	public static AdvancementBookWorldData get(ServerLevel level) {
		DimensionDataStorage dataManager = level.getDataStorage();

		return dataManager.computeIfAbsent(AdvancementBookWorldData::read, AdvancementBookWorldData::new, DATA_NAME);
	}
	
	@Override
	@Nonnull
	@ParametersAreNonnullByDefault
	public CompoundTag save(CompoundTag compound) {
		ListTag nbtGivenBook = new ListTag();
		for(UUID uuid : this.givenBook) {
			nbtGivenBook.add(StringTag.valueOf(uuid.toString()));
		}
		compound.put("givenBook", nbtGivenBook);
		return compound;
	}

	private static AdvancementBookWorldData read(CompoundTag nbt) {
		AdvancementBookWorldData data = new AdvancementBookWorldData();
		data.givenBook.clear();
		ListTag nbtGivenBook = nbt.getList("givenBook", 8);
		for(int i=0;i < nbtGivenBook.size();i++) {
			data.givenBook.add(UUID.fromString(nbtGivenBook.getString(i)));
		}
		return data;
	}
}
