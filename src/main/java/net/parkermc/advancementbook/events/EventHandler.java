package net.parkermc.advancementbook.events;

import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.parkermc.advancementbook.AdvancementBookMod;
import net.parkermc.advancementbook.AdvancementBookConfig;
import net.parkermc.advancementbook.data.AdvancementBookWorldData;
import net.parkermc.advancementbook.items.AdvancementBookItems;

@Mod.EventBusSubscriber(modid = AdvancementBookMod.MODID)
public class EventHandler {

	@SubscribeEvent
	public static void joinWorldEvent(EntityJoinWorldEvent event) {
		if(AdvancementBookConfig.GIVE_FIRST_JOIN.get() && event.getEntity() instanceof Player && (!event.getWorld().isClientSide())) {
			AdvancementBookWorldData data = AdvancementBookWorldData.get((ServerLevel)event.getWorld());

			if(!data.givenBook.contains(event.getEntity().getUUID())) {
				((Player)event.getEntity()).getInventory().add(new ItemStack(AdvancementBookItems.ADVANCEMENT_BOOK.get(), 1));
				data.givenBook.add(event.getEntity().getUUID());
				data.setDirty();
			}
		}
	}
}
